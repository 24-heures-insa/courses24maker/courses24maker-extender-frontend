import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Athletes from "../views/Athletes.vue";
import Payments from "@/views/Payments.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Athletes",
    component: Athletes,
  },
  {
    path: "/payments",
    name: "Payments",
    component: Payments,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
