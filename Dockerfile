FROM node:lts-alpine as build-stage

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build


FROM node:16.14-alpine3.15

COPY --from=build-stage /app/dist /app

WORKDIR /app

RUN npm install serve

EXPOSE 80

CMD ["./node_modules/.bin/serve", "--single", "--listen", "80", "./"]
